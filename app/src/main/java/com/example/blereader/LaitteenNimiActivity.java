package com.example.blereader;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class LaitteenNimiActivity extends AppCompatActivity
{
BluetoothDevice objReceived;
BluetoothGatt bluetoothGatt;
String yhteys = "Ei yhteyttä.";
boolean b = false;
String gattPalvelut = "";

    // Device connect call back
BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback()
    {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState)
        {
            yhteys = "Yhteyden tila muuttuu";
            if (newState == BluetoothProfile.STATE_CONNECTED)
            {
                // successfully connected to the GATT Server
                yhteys = "GATT yhdistetty!";

                b=bluetoothGatt.discoverServices();
                if (b) {
                    List<BluetoothGattService> gattServices = bluetoothGatt.getServices();
                    int pituus = gattServices.size();
                    yhteys = "Serviceslista saatu";
                    if (pituus > 0) {
                        Integer pi = pituus;
                        String pit = pi.toString();
                        yhteys = "Serviceslistan koko: " + pit;

                        // Loops through available GATT Services.
                        for (BluetoothGattService gattService : gattServices) {
                            final String uuid = gattService.getUuid().toString();
                            gattPalvelut += "\n" + uuid;
                        }
                    }
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED)
            {
                // disconnected from the GATT Server
                yhteys = "GATT disconnected.";
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laitteen_nimi);

        objReceived = (BluetoothDevice)((ObjectWrapperForBinder)getIntent().getExtras().getBinder("laite")).getData();
        String laitenimi = objReceived.getName();
        if (laitenimi == null)
        {
            laitenimi = "Laite ei kerro nimeään.";
        }
        String osoite = objReceived.toString();
        String textToDisplay = laitenimi + "\n" + osoite;

        TextView name = (TextView)findViewById(R.id.nimitextView);
        name.setText(textToDisplay);

    } // onCreate

    @Override
    protected void onDestroy()
    {
        bluetoothGatt.close();
        bluetoothGatt = null;
        super.onDestroy();

    }

    public void onClickYhdista (View view)
    {
        String teksti = "Yhdistetään laitteeseen " + objReceived.toString();
        Toast.makeText(this,teksti, Toast.LENGTH_LONG).show();
        bluetoothGatt = objReceived.connectGatt(this, false,bluetoothGattCallback);
        if (bluetoothGatt==null)
        {
            yhteys = " GATT yhteydenmuodostus ei onnistu.";
        }

        Toast.makeText(this,yhteys, Toast.LENGTH_LONG).show();
        TextView name = (TextView)findViewById(R.id.nimitextView);
        name.setText(yhteys);

    }

}