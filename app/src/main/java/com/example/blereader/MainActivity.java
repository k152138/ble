package com.example.blereader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    String teksti = "";
    ArrayAdapter<BluetoothDevice> mNewDevicesArrayAdapter;
    BluetoothLeScanner scanner;



   /* Classic Bluetooth:

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                BluetoothDevice btDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = btDevice.getName();
                boolean flag = true;    // flag to indicate that particular device is already in the arlist or not
                for(int i = 0; i<mNewDevicesArrayAdapter.getCount();i++)
                    {
                        if(btDevice.equals(mNewDevicesArrayAdapter.getItem(i)))
                        {
                            flag = false;
                        }
                    }
                    if(flag)
                    {
                        mNewDevicesArrayAdapter.add(btDevice);
                        mNewDevicesArrayAdapter.notifyDataSetChanged();
                    }

            }
        }
    };

    */

    // BLE:
    private final ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice btDevice = result.getDevice();

            String deviceName = btDevice.getName();
            boolean flag = true;    // flag to indicate that particular device is already in the arlist or not
            for(int i = 0; i<mNewDevicesArrayAdapter.getCount();i++)
            {
                if(btDevice.equals(mNewDevicesArrayAdapter.getItem(i)))
                {
                    flag = false;
                }
            }
            if(flag)
            {
                mNewDevicesArrayAdapter.add(btDevice);
                mNewDevicesArrayAdapter.notifyDataSetChanged();
            }

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            // Ignore for now
        }

        @Override
        public void onScanFailed(int errorCode) {
            // Ignore for now
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                BluetoothDevice selected = mNewDevicesArrayAdapter.getItem(position-1);

                final Bundle bundle = new Bundle();
                bundle.putBinder("laite", new ObjectWrapperForBinder(selected));
                startActivity(new Intent(MainActivity.this, LaitteenNimiActivity.class).putExtras(bundle));

            }
        };


        BluetoothAdapter BT = BluetoothAdapter.getDefaultAdapter();
        if (BT == null) {
            teksti = "Laite ei tue Bluetoothia!";
        } else if (BT.isEnabled()) {
            teksti = "Bluetooth päällä.";
        } else {
            teksti = "Bluetooth pois päältä.";
        }

        Toast.makeText(this, teksti, Toast.LENGTH_LONG).show();

        ListView newDevicesListView = findViewById(R.id.pairedBtDevices);
        View headerView = getLayoutInflater().inflate(R.layout.listview_header2, null);
        newDevicesListView.addHeaderView(headerView);

        mNewDevicesArrayAdapter = new ArrayAdapter<BluetoothDevice>(this, android.R.layout.simple_list_item_1);
        newDevicesListView.setAdapter(mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(itemClickListener);


        // Classic Bluetooth:
     /*   //Seuraavat laitteet on yhdistetty:
        Set<BluetoothDevice> pairedDevices = BT.getBondedDevices();
        if (pairedDevices.size() > 0)
        {
            for (BluetoothDevice d : pairedDevices)
            {
                String deviceName = d.getName();
                mNewDevicesArrayAdapter.add(d);
            }
        }


        //Seuraavat laitteet ovat saatavilla:

        // IntentFilter for found devices
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

        // Broadcast receiver for any matching filter
        registerReceiver(mReceiver, filter);
        boolean haku=false;
        try {
                haku = BT.startDiscovery();
            }
            catch (NullPointerException npe)
            {
                Toast.makeText(this, "Haku kaatui.", Toast.LENGTH_LONG).show();
            }

        if (haku)
        {
            Toast.makeText(this, "Haku käynnissä.", Toast.LENGTH_LONG).show();
        }

        if (!haku)
        {
            Toast.makeText(this, "Haku ei onnistunut.", Toast.LENGTH_LONG).show();
        }


      */


        //BLE:
        ScanSettings scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
                .setReportDelay(0L)
                .build();
        scanner = BT.getBluetoothLeScanner();
        if (scanner != null)
        {
            scanner.startScan(null, scanSettings, scanCallback);
            Toast.makeText(this, "Haku alkoi.", Toast.LENGTH_LONG).show();
        }
        else
            {
                Toast.makeText(this, "Haku ei onnistunut.", Toast.LENGTH_LONG).show();
            }


    }  //onCreate

    @Override
    protected void onDestroy()
    {
        scanner.stopScan(scanCallback);
        super.onDestroy();

    }
/*
    public BluetoothDevice getBTDevice(String deviceName)
    {
        for(int i = 0; i<mNewDevicesArrayAdapter.getCount();i++)
        {
            BluetoothDevice device = mNewDevicesArrayAdapter.getItem(i);
            if(deviceName.equals(device.getName()))
            {
                return device;
            }
        }
        return null;
    }
*/

}